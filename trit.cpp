#include "trit.h"

#include <iostream>

constexpr Trit::State Trit::sum [3][3][2];
constexpr Trit::State Trit::mul [3][3];

Trit::Trit() {}

Trit::Trit(const Trit::State &s) : st(s) {}

Trit::~Trit() {}

Trit Trit::operator= (const Trit::State &s)
{
    st = s;
    return *this;
}

Trit Trit::operator=(const Trit &t)
{
    st = t.st;
    return *this;
}

Trit Trit::operator=(const int &n)
{
    if(n == Trit::T) st = Trit::T;
    else if (n == Trit::I) st = Trit::I;
    else st = Trit::O;

    return *this;
}

Trit Trit::operator+(const Trit &t) const
{
    return Trit(Trit::sum[t.st + 1][st + 1][0]);
}

Trit Trit::getRem(const Trit &t) const
{
    return Trit(sum[t.st + 1][st + 1][1]);
}

Trit Trit::operator*(const Trit &t) const
{
    return Trit(mul[t.st + 1][st + 1]);

    return *this;
}

Trit::State Trit::get() const
{
    return st;
}

