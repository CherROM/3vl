#ifndef TRIT_H
#define TRIT_H

class Trit
{ 
public:
    enum State {
        T = -1,
        O = 0,
        I = 1,
    };
private:
    static constexpr Trit::State sum [3][3][2] = {
        {{I, T}, {T, O}, {O, O}},
        {{T, O}, {O, O}, {I, O}},
        {{O, O}, {I, O}, {T, I}}
    };
    static constexpr Trit::State mul [3][3] = {
        {I, O, T},
        {O, O, O},
        {T, O, I}
    };

    State st = O;    
public:
    Trit();
	Trit(const Trit::State& s);
    ~Trit();
	Trit operator= (const State& s);
	Trit operator= (const Trit& t);
	Trit operator= (const int& n);

	Trit operator+ (const Trit& t) const;
	Trit operator* (const Trit& t) const;

	Trit getRem (const Trit& t) const;

    State get() const;
};

#endif // TRIT_H
