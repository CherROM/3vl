#include "tryte.h"

std::map<char, Trit::State> Tryte::toState= {
    {'-', Trit::T},
    {'0', Trit::O},
    {'+', Trit::I}
};
std::map<Trit::State, char> Tryte::toChar = {
    {Trit::T, '-'},
    {Trit::O, '0'},
    {Trit::I, '+'}
};

Tryte::Tryte() {}

Tryte::Tryte(const char c[])
{
    operator= (c);
}

Tryte::Tryte(const int &n)
{
    operator= (n);
}

Tryte::~Tryte()
{

}

Tryte Tryte::operator+(const Tryte &n) const
{
    Tryte result;
	Tryte carry;

    for(int i = size-1; i >= 0; --i)
    {
        result[i] = cnt[i] + n.cnt[i];
		carry[i] = cnt[i].getRem(n.cnt[i]);
    }
	if(carry.empty().get() == Trit::T) result += (carry << 1);

    return result;
}

Tryte Tryte::operator-() const
{
    Tryte res;

    for(int i = 0; i < size; ++i) {
        res[i] = cnt[i] * Trit(Trit::T);
    }
    return res;
}

Tryte Tryte::operator-(const Tryte &n) const
{
    return operator+ (-n);
}

Tryte Tryte::operator*(const Tryte &n) const
{
    Tryte temp[size];
    Tryte res;

    for(int i = 0; i < size; ++i)
    {
        for(int j = size - 1; j >= 0; --j)
        {
            temp[i][j] = cnt[j] * n.cnt[size-1 - i];
        }
    }

    for(short i = 0; i < size; ++i)
    {
        res += (temp[i] << i);
    }

    return res;
}

Tryte Tryte::operator/(const Tryte &n) const
{
    Tryte divider = n;
    Tryte doubleDivider = n*2;
    Tryte doubleDividend = operator*(2);

    Tryte temp;
    Tryte res;

    if(divider.sgn() < 0) divider.invert();

	if((divider.abs() > 0).get() == Trit::I)
    {
        for(int i = 0; i < size; ++i)
        {
            temp.insert(doubleDividend[i]);

			if(((temp.abs() > divider.abs()).get() == Trit::I)
					|| (((temp.abs() == divider.abs()).get() == Trit::I)
                    && (doubleDividend.sgnk(i+1) == temp.sgn())))
            {
                if(temp.sgn() == divider.sgn())
                {
                    temp -= doubleDivider;
                    res.insert(Trit::I);
                }
                else
                {
                    temp += doubleDivider;
                    res.insert(Trit::T);
                }
            }
            else res.insert(Trit::O);
        }
        if((temp < 0).get() == Trit::I && sgn() > 0) res -= n.sgn();
        else if((temp > 0).get() == Trit::I && sgn() < 0) res += n.sgn();
    }

    return res;
}

Tryte Tryte::operator+= (const Tryte &n)
{
    operator= (operator+ (n));
    return *this;
}

Tryte Tryte::operator-=(const Tryte &n)
{
    operator= (operator- (n));
    return *this;
}

Tryte Tryte::operator*= (const Tryte &n)
{
    operator= (operator* (n));
    return *this;
}

Tryte Tryte::operator/=(const Tryte &n)
{
    operator= (operator/ (n));
    return *this;
}

Tryte Tryte::operator++()
{
    operator+=(1);

    return *this;
}

Tryte Tryte::operator++(int)
{
    auto res = *this;

    operator+=(1);

    return res;
}

Tryte Tryte::operator--()
{
    operator-=(1);

    return *this;
}

Tryte Tryte::operator--(int)
{
    auto res = *this;

    operator-=(1);

    return res;
}

Trit Tryte::operator==(const Tryte &n) const
{
    for(short i = 0; i < size; ++i)
    {
        if(cnt[i].get() != n.cnt[i].get()) return Trit(Trit::T);
    }
    return Trit(Trit::I);
}

Trit Tryte::operator!=(const Tryte &n) const
{
    for(short i = 0; i < size; ++i)
    {
        if(cnt[i].get() != n.cnt[i].get()) return Trit(Trit::I);
    }
    return Trit(Trit::T);
}

Trit Tryte::operator<(const Tryte &n) const
{
    for(short i = 0; i < size; ++i)
    {
        if(cnt[i].get() < n.cnt[i].get()) return Trit(Trit::I);
        else if (cnt[i].get() > n.cnt[i].get()) return Trit(Trit::T);
    }
	return Trit(Trit::O);
}

Trit Tryte::operator>(const Tryte &n) const
{
    for(short i = 0; i < size; ++i)
    {
        if(cnt[i].get() > n.cnt[i].get()) return Trit(Trit::I);
        else if (cnt[i].get() < n.cnt[i].get()) return Trit(Trit::T);
    }
	return Trit(Trit::O);
}

Tryte Tryte::operator<<(const int &n)
{
    if(n >= 1)
    {
        for(int i = n; i < size; ++i) cnt[i - n] = cnt[i];
        for(int i = size - n; i < size; ++i) cnt[i] = Trit::O;
    }

    return *this;
}

Tryte Tryte::operator>>(const int &n)
{
    if(n >= 1)
    {
        for(int i = size-1; i > 0; --i) cnt[i] = cnt[i-n];
        for(int i = 0; i < n; i++) cnt[i] = Trit::O;
    }

    return *this;
}

Trit& Tryte::operator[](const int &i)
{
    return cnt[i];
}

Trit Tryte::empty() const
{
    for(auto i : cnt) {
        if(i.get() != Trit::O) return Trit(Trit::T);
    }
    return Trit(Trit::O);
}

std::string Tryte::toString() const
{
    std::string res;

    for(short i = 0; i < size; ++i)
    {
        res += toChar[cnt[i].get()];
    }

    return res;
}

int Tryte::toInt() const
{
    int res = 0;
	int exp = 1;

	for(short i = size-1; i >= 0; --i)
    {
		res += (cnt[i].get() * exp);
		exp *= 3;
    }

    return res;
}

Tryte Tryte::invert() const
{
    return operator-();
}

Tryte Tryte::insert(const Trit &t)
{
    for(int i = 0; i < size-1; ++i) cnt[i] = cnt[i + 1];
	cnt[size-1] = t;

    return *this;
}

Tryte Tryte::abs() const
{
	if(operator <(0).get() == Trit::I) return invert();
	else return *this;
}

int Tryte::sgn() const
{
	for(auto i : cnt) if(i.get() != Trit::O) return i.get();

    return 1;
}

int Tryte::sgnk(const int &n) const
{
    if (n >= 0)
    {
        int i = n;
        while (cnt[i].get() == Trit::O && i++ < size);
        if (i < size) return cnt[i].get();
    }

    return 0;
}

Tryte Tryte::operator= (const char c[])
{
    short pos = 0;

    for(auto i = 0; i < size; ++i) if(c[i] == '\0') {pos = size - i; break;}
    for(auto i = 0; i < size; ++i) cnt[i] = (i < pos) ? Trit::O : toState[c[i - pos]];

    return *this;
}

Tryte Tryte::operator=(const std::string &s)
{
    if(s.substr(0, 2) == "0t")
    {
        operator=(s.substr(2).c_str());
    }
    else operator=(std::stoi(s));

    return *this;
}

Tryte Tryte::operator= (const Tryte &t)
{
    for(short i = 0; i < size; ++i)
    {
        cnt[i] = t.cnt[i];
    }

    return *this;
}

Tryte Tryte::operator=(const int &n)
{
    int temp = n;

    for(short i = size - 1; i >= 0; --i)
    {
		if(std::abs(temp % 3) > 1)
        {
            cnt[i] = (temp > 0) ? -1 : 1;
            (temp += (temp > 0) ? 1 : -1) /= 3;
        }
        else
        {
            cnt[i] = temp % 3;
            temp /= 3;
        }
    }

    return *this;
}

