#include <iostream>
#include "tryte.h"
#include "vm.h"

int main(int argc, char *argv[])
{
    std::string filename;

    if(argc > 1) filename = argv[1];
    else
    {
        std::cout << "Enter Filename:" << std::endl;
        std::cin >> filename;
    }

    VM v1(filename);

//    Tryte t1 = 7;
//    Tryte t2 = 2;

//    std::cout << t1.toString() << " (" << t1.toInt() << ")" << std::endl;
//    std::cout << t2.toString() << " (" << t2.toInt() << ")" << std::endl;

//    Tryte t3 = ++t1;

//    std::cout << t3.toString() << " (" << t3.toInt() << ")" << std::endl;

//    int m;
//    std::cin >> m;

    return 0;
}
