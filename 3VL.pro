#-------------------------------------------------
#
# Project created by QtCreator 2015-04-15T23:24:59
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = 3VL
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    tryte.cpp \
    trit.cpp \
    vm.cpp

HEADERS += \
    tryte.h \
    trit.h \
    vm.h

CONFIG += c++11
