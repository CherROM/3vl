#include "vm.h"

void VM::exec()
{
    std::string bits[maxBits];
    std::string str;

    while(src.peek() != EOF)
    {
        getline(src, str);

        if (str.front() == '!') splitEcho(str, bits);
		else if (str.front() == 's') splitS(str, bits);
        else split(str, bits);

        if (bits[0] == "#" || bits[0] == ":") continue;

        (this->*functions[bits[0]])(bits);
    }
}

void VM::split(const std::string &str, std::string *bits)
{
    for(short int i = 0; i != maxBits; ++i) bits[i].clear();

    unsigned short int j = 0;
	for(unsigned short int i = 0; str[i] != '\0' && i < maxLen; ++i)
    {
        if(str[i] == ' ' && str[i - 1] != ' ') ++j;
        if(str[i] != ' ') bits[j].push_back(str[i]);
    }
}

void VM::splitEcho(const std::string &str, std::string *bits)
{
    for(short int i = 0; i != maxBits; ++i) bits[i].clear();

    bits[0] = '!';

	for(unsigned short int i = 2; str[i] != '\0' && i < maxLen; ++i) bits[1].push_back(str[i]);
}

void VM::splitS(const std::string& str, std::string* bits)
{
	for(short int i = 0; i != maxBits; ++i) bits[i].clear();

	unsigned short int j = 0;
	for(unsigned short int i = 0; str[i] != '\0' && i < maxLen; ++i)
	{
		if(str[i] == ' ' && str[i - 1] != ' ' && j < 2) ++j;
		if(str[i] != ' ' || j >= 2) bits[j].push_back(str[i]);
	}
	bits[2] = bits[2].substr(1);
}

void VM::preprocess()
{
    unsigned int line = 0;

    std::string bits[maxBits];
    std::string str;

    while(src.peek() != EOF)
    {
        getline(src, str);
        ++line;

        if (str.front() == ':')
        {
            split(str, bits);
            labels[bits[1]] = line;
        }
    }
    gtl(0);
    exec();
}

void VM::gtl(const int &ln)
{
    unsigned int line = 0;
    src.seekg(std::ios::beg);
    for(int i = 0; i < ln; ++i)
    {
        ++line;
        src.ignore(maxLen, '\n');
    }
}

void VM::add(const std::string *bits)
{
    vars[bits[1]] = bits[2];
}

void VM::sum(const std::string *bits)
{
    if(bits[3].empty()) vars[bits[1]] += vars[bits[2]];
    else vars[bits[3]] = vars[bits[1]] + vars[bits[2]];
}

void VM::sub(const std::string *bits)
{
    if(bits[3].empty()) vars[bits[1]] -= vars[bits[2]];
    else vars[bits[3]] = vars[bits[1]] - vars[bits[2]];
}

void VM::mul(const std::string *bits)
{
    if(bits[3].empty()) vars[bits[1]] *= vars[bits[2]];
    else vars[bits[3]] = vars[bits[1]] * vars[bits[2]];
}

void VM::div(const std::string *bits)
{
    if(bits[3].empty()) vars[bits[1]] /= vars[bits[2]];
    else vars[bits[3]] = vars[bits[1]] / vars[bits[2]];
}

void VM::dsp(const std::string *bits)
{
    std::cout << bits[1] << " = "<< vars[bits[1]].toString()  << " (" << vars[bits[1]].toInt() << ")" << std::endl;
}

void VM::cnd(const std::string *bits)
{
    if((vars[bits[1]].empty().get() == Trit::I)) gtl(labels[bits[2]]);
}

void VM::inc(const std::string *bits)
{
    ++vars[bits[1]];
}

void VM::dec(const std::string *bits)
{
    --vars[bits[1]];
}

void VM::echo(const std::string *bits)
{
	std::cout << bits[1] << "\n";
}

void VM::adds(const std::string* bits)
{
	strings[bits[1]] = bits[2];
}

void VM::prints(const std::string* bits)
{
	std::cout << strings[bits[1]] << std::endl;
}

void VM::sums(const std::string* bits)
{
	strings[bits[1]] += strings[bits[2]];
}

void VM::trit2s(const std::string* bits)
{
	strings[bits[2]] = vars[bits[1]].toString();
}

void VM::s2trit(const std::string* bits)
{
	vars[bits[2]] = strings[bits[1]];
}

void VM::operator()(const std::string &f)
{
    src.open(f, std::fstream::in);

    if(src.is_open())
    {
        preprocess();

        src.close();
    }
    else std::cout << "File Not Found" << '\n';
}

VM::VM()
{
    functions =
    {
		{"set", &VM::add},
		{"+", &VM::sum},
		{"-", &VM::sub},
		{"*", &VM::mul},
		{"/", &VM::div},
		{"out", &VM::dsp},
		{"goto", &VM::cnd},
		{"++", &VM::inc},
		{"--", &VM::dec},
		{"!", &VM::echo},
		{"sets", &VM::adds},
		{"prints", &VM::prints},
		{"sums", &VM::sums},
		{"trit2s", &VM::trit2s},
		{"s2trit", &VM::s2trit}
    };
}

VM::VM(const std::string &f)
{
	functions =
	{
		{"set", &VM::add},
		{"+", &VM::sum},
		{"-", &VM::sub},
		{"*", &VM::mul},
		{"/", &VM::div},
		{"out", &VM::dsp},
		{"goto", &VM::cnd},
		{"++", &VM::inc},
		{"--", &VM::dec},
		{"!", &VM::echo},
		{"sets", &VM::adds},
		{"prints", &VM::prints},
		{"sums", &VM::sums},
		{"trit2s", &VM::trit2s},
		{"s2trit", &VM::s2trit}
	};
    operator() (f);
}

VM::~VM()
{
    if(src.is_open()) src.close();
}
