﻿#ifndef TRYTE_H
#define TRYTE_H

#include "trit.h"
#include <string>
#include <map>

class Tryte
{
private:
    static const short size = 9;
    Trit cnt[size];

    static std::map<char, Trit::State> toState;
    static std::map<Trit::State, char> toChar;

public:
    Tryte();
    Tryte(const char c[]);
	Tryte(const int& n);
    ~Tryte();

	Tryte operator=(const char c[]);
	Tryte operator=(const std::string& s);
	Tryte operator=(const Tryte& t);
	Tryte operator=(const int& n);

	Tryte operator-() const;
	Tryte operator+(const Tryte& n) const;
	Tryte operator-(const Tryte& n) const;
	Tryte operator*(const Tryte& n) const;
	Tryte operator/(const Tryte& n) const;

	Tryte operator+=(const Tryte& n);
	Tryte operator-=(const Tryte& n);
	Tryte operator*=(const Tryte& n);
	Tryte operator/=(const Tryte& n);

	Tryte operator++();
	Tryte operator++(int);

	Tryte operator--();
	Tryte operator--(int);

	Trit operator==(const Tryte& n) const;
	Trit operator!=(const Tryte& n) const;

	Trit operator<(const Tryte& n) const;
	Trit operator>(const Tryte& n) const;

	Tryte operator<<(const int& n);
	Tryte operator>>(const int& n);

	Trit& operator[](const int& i);

    Trit empty() const;

    std::string toString() const;
	int toInt() const;

    Tryte invert() const;
	Tryte insert(const Trit& t);
	Tryte abs() const;

    int sgn() const;
	int sgnk(const int& n) const;
};

#endif // TRYTE_H
