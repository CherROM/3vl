#ifndef CALC_H
#define CALC_H

#include <string>
#include <iostream>
#include <fstream>
#include <unordered_map>
#include "tryte.h"

class VM
{
private:
    typedef void (VM::*Op)(const std::string* bits);

    const short int maxBits = 4;
	const int maxLen = 64;

    std::fstream src;

    std::unordered_map<std::string, Tryte> vars;
	std::unordered_map<std::string, std::string> strings;
	std::unordered_map<std::string, unsigned int> labels;
    std::unordered_map<std::string, Op> functions;

    void exec();

    void split(const std::string& str, std::string* bits);
    void splitEcho(const std::string& str, std::string* bits);	
	void splitS(const std::string& str, std::string* bits);

    void preprocess();

    void gtl(const int& ln);

	void add(const std::string* bits);
    void sum(const std::string* bits);
    void sub(const std::string* bits);
    void mul(const std::string* bits);
    void div(const std::string* bits);
    void dsp(const std::string* bits);
    void lbl(const std::string* bits);
    void cnd(const std::string* bits);
    void inc(const std::string* bits);
    void dec(const std::string* bits);
    void echo(const std::string* bits);

	void adds(const std::string* bits);
	void prints(const std::string* bits);
	void sums(const std::string* bits);

	void trit2s(const std::string* bits);
	void s2trit(const std::string* bits);

public:
	void operator() (const std::string& f);

    VM();
	VM(const std::string& f);
    ~VM();
};

#endif // CALC_H
